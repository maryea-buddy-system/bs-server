#!/bin/bash

SERVICES=(gateway)

for service in "${SERVICES[@]}"
do
  cd services/$service && npm i && cd -
done