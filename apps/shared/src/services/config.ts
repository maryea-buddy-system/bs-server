import "dotenv/config";
import { RmqOptions, Transport } from "@nestjs/microservices";


export class ConfigService {
  private readonly envConfig: { [key: string]: any} = null;

  constructor() {
    this.envConfig = {};

    this.envConfig.gatewayPort = process.env.GATEWAY_PORT || 8000;

    const user = process.env.RABBITMQ_USER || "guest";
    const password = process.env.RABBITMQ_PASSWORD || "guest";
    const port = process.env.RABBITMQ_PORT || 5672;

    this.envConfig.apiService = {
      transport: Transport.RMQ,
      options: {
        urls: [`amqp://${user}:${password}@localhost:${port}`],
        queue: process.env.API_QUEUE || "api",
        queueOptions: {
          durable: true
        }
      }
    } as RmqOptions;

    this.envConfig.db = {
      type: "postgres",
      host: "localhost",
      port: process.env.POSTGRES_PORT || 5432,
      username: process.env.POSTGRES_USER || "postgres",
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB || "buddysystem",
      entities: [__dirname + "**/*.entity.ts"],
      synchronize: true
    };
  }

  get(key: string): any {
    return this.envConfig[key];
  }
}