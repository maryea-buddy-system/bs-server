import { Module } from "@nestjs/common";
import { ConfigService } from "./services";

@Module({
  imports: [],
  controllers: [],
  exports: [ConfigService],
  providers: [ConfigService],
})
export class SharedModule {}
