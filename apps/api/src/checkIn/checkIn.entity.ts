import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class CheckIn {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;
}