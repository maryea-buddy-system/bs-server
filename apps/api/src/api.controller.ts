import { Controller } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { ApiService } from "./api.service";

@Controller()
export class ApiController {
  constructor(private readonly apiService: ApiService) {}

  @MessagePattern("hello_world")
  async getHello(name: string): Promise<string> {
    return this.apiService.getHello(name);
  }
}
