import { NestFactory } from "@nestjs/core";
import { MicroserviceOptions } from "@nestjs/microservices";

import { ConfigService } from "apps/shared/src";

import { ApiModule } from "./api.module";

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    ApiModule,
    new ConfigService().get("apiService")
  );

  await app.listen();
}
bootstrap();
