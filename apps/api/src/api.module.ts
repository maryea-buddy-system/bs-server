import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { ConfigService, SharedModule } from "apps/shared/src";

import { ApiController } from "./api.controller";
import { ApiService } from "./api.service";

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [SharedModule],
      inject: [ConfigService],
      useFactory: async(configService: ConfigService) => configService.get("db")
    })
  ],
  controllers: [ApiController],
  providers: [
    ConfigService,
    ApiService
  ],
})
export class ApiModule {}
