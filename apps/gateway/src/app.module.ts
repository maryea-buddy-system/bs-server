import { Module } from "@nestjs/common";
import { ClientProxyFactory } from "@nestjs/microservices";

import { ConfigService, SharedModule } from "apps/shared/src";

import { AppController } from "./app.controller";

@Module({
  imports: [SharedModule],
  controllers: [AppController],
  providers: [
    {
      provide: "API_SERVICE",
      useFactory: (configService: ConfigService) => {
        const apiServiceOptions = configService.get("apiService");
        return ClientProxyFactory.create(apiServiceOptions);
      },
      inject: [ConfigService]
    }
  ],
})
export class AppModule {}
