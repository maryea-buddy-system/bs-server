import { NestFactory } from "@nestjs/core";

import { ConfigService } from "apps/shared/src";

import { AppModule } from "./app.module";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const configService = app.get(ConfigService)

  await app.listen(configService.get("gatewayPort"));
}
bootstrap();
