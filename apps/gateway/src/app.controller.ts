import { Controller, Get, Inject } from "@nestjs/common";
import { ClientProxy } from "@nestjs/microservices";
import { firstValueFrom } from "rxjs";

@Controller()
export class AppController {
  constructor(@Inject("API_SERVICE") private apiServiceClient: ClientProxy){}

  @Get()
  async getHello(): Promise<string> {

    const response = await firstValueFrom(
      this.apiServiceClient.send("hello_world", "Eric")
    );
    return response;
  }

}
